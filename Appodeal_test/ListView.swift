//
//  ListView.swift
//  Appodeal_test
//
//  Created by Крамич on 5/1/19.
//  Copyright © 2019 Kramich. All rights reserved.
//

import UIKit
import Appodeal
import ASExtentions

protocol NativeViewDelegate{
    func presentNative(onView view: UIView, fromIndex index: NSIndexPath)
}

class ListView: UIView {

    @IBOutlet weak var listTableView: UITableView!
    var delegate : NativeViewDelegate?
    
    let nativeAdStack : NSMapTable <NSIndexPath, APDNativeAd> = NSMapTable(keyOptions: .strongMemory, valueOptions: .strongMemory)
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        listTableView.delegate = self
        listTableView.dataSource = self
    }

}

extension ListView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 110
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row % 10 == 0 {
            let cell = UITableViewCell()
            delegate?.presentNative(onView: cell.contentView, fromIndex: indexPath as NSIndexPath)
            return cell
        }else{
            let cell = UITableViewCell()
            cell.textLabel?.text = "\(indexPath.row-Int(floor(Double(indexPath.row / 10))))"
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row % 10 == 0 {
            return 400
        }else{
            return 44
        }
    }
    
}


