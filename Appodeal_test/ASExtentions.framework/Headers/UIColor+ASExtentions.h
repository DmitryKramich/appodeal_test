//
//  UIColor+ASExtentions.h
//
//  Copyright © 2018 Appodeal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (ASExtentions)

+ (UIColor *)asxMainColor;

- (UIImage *)asxImage;

@end
