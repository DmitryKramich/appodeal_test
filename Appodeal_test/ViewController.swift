//
//  ViewController.swift
//  Appodeal_test
//
//  Created by Крамич on 5/1/19.
//  Copyright © 2019 Kramich. All rights reserved.
//

import UIKit
import Appodeal
import ASExtentions

class ViewController: UIViewController,NativeViewDelegate {

    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var stopAds: UIButton!
    
    var counter = 30
    var isLoadAds = true
    var isStopTimer = true
    var timer:Timer?
    var adQueue : APDNativeAdQueue = APDNativeAdQueue()
    
    var  listView = (UINib(nibName: String(describing: ListView.self), bundle: Bundle(for: ListView.self)).instantiate(withOwner: nil, options: nil).first as? ListView)!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Appodeal.setInterstitialDelegate(self)
        Appodeal.setBannerDelegate(self)
        Appodeal.showAd(.bannerTop, rootViewController: self)
        adQueue.settings.adViewClass = NativeView.self
        adQueue.autocache = true
        self.adQueue.delegate = self
        adQueue.loadAd()
        setTimer()
    }
    
    private func setTimer() {
        self.timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { (_) in
            self.counter -= 1
            if self.counter == 0 {
                self.timer?.invalidate()
                self.timer = nil
                if self.isStopTimer {
                    self.isStopTimer = !self.isStopTimer
                    self.counter = 0
                    self.stopAds.setTitle("load list", for: .normal)
                }
                if self.isLoadAds{
                    Appodeal.showAd(.interstitial,  rootViewController: self)
                }
            }
            self.timerLabel.text = "\(self.counter)"
        }
    }
    
    @IBAction func stopAdsPressed(_ sender: UIButton) {
        if !isLoadAds || !isStopTimer{
            self.timer?.invalidate()
            self.timer = nil
            listView.delegate = self
            self.view = listView
        }
        if isStopTimer {
            isLoadAds = false
            self.stopAds.setTitle("load list", for: .normal)
        }
    }

    func presentNative(onView view: UIView, fromIndex index: NSIndexPath) {
        var nativeAd: APDNativeAd?;
        
        if listView.nativeAdStack.object(forKey: index) != nil {
            nativeAd = listView.nativeAdStack.object(forKey: index)
        } else if adQueue.currentAdCount > 0 {
            nativeAd = adQueue.getNativeAds(ofCount: 1).first
        }
        
        guard nativeAd == nil else {
            let nativeView = nativeAd!.getViewFor(self)
            view.addSubview(nativeView!)
            nativeView!.asxEdgesEqualView(view)
            return
        }
    }
    
}

extension ViewController: AppodealInterstitialDelegate {

    func interstitialDidLoadAdIsPrecache(_ precache: Bool) {
        
    }
    
    func interstitialDidFailToLoadAd() {
        
    }
    
    func interstitialDidFailToPresent() {

    }
    
    func interstitialWillPresent() {
        
    }
    
    func interstitialDidDismiss() {
        self.counter = 30
        setTimer()
    }
    
    func interstitialDidClick() {
        
    }
    
    func interstitialDidExpired(){
        
    }

}

extension ViewController:AppodealBannerDelegate{
    
    func bannerDidLoadAdIsPrecache(_ precache: Bool){
       
    }
    
    func bannerDidFailToLoadAd(){
      
    }
    
    func bannerDidClick(){
       
    }
    
    func bannerDidShow(){
        Timer.scheduledTimer(withTimeInterval: 5, repeats: false) { (_) in
            Appodeal.hideBanner()
        }
    }
    
    func bannerDidExpired(){
       
    }
}

extension ViewController : APDNativeAdPresentationDelegate {
    
    func nativeAdWillLogImpression(_ nativeAd: APDNativeAd) {
        
    }
    
    func nativeAdWillLogUserInteraction(_ nativeAd: APDNativeAd) {
        
    }
    
}

extension ViewController : APDNativeAdQueueDelegate {

    
    func adQueue(_ adQueue: APDNativeAdQueue, failedWithError error: Error) {
    }
    
    func adQueueAdIsAvailable(_ adQueue: APDNativeAdQueue, ofCount count: UInt) {
       
    }
    
}
